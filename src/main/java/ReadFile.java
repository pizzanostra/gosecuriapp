import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class ReadFile {


    //Method which returns a Tools object Arraylist.
    public ArrayList<Tools> readTools(String path) {

        // On lis le fichier
        ArrayList<Tools> ar = new ArrayList<>(); // creation of a Tools ArrayList.
        try {
            FileReader fr = new FileReader(path); // creation of a FileReader object fr.
            BufferedReader br = new BufferedReader(fr); // creation of a BufferedReader object br.
            String line; // declaration of an object String line.
            while ((line = br.readLine()) != null) {
                //beginning of the while loop
                String[] data = line.split("\t"); //Getting data, assigning it to each indexes of the Array String[]
                Tools tools = new Tools(data[0], data[1]); // We create an a tools object; which is composed by
                ar.add(tools);
            }

            fr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return ar;
    }

        public ArrayList<String> readAgentList(String path){

            ArrayList<String> ar = new ArrayList<>();
            try {
                FileReader fr = new FileReader(path);
                BufferedReader br = new BufferedReader(fr);
                String line;
                while ((line = br.readLine()) != null) {

                    ar.add(line);
                }
                fr.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return ar;
        }

        public Agent readAgent(String path, String agentId){

            Agent ag  = new Agent();
            ag.setId(agentId);

            try {
                FileReader fr = new FileReader(path);
                BufferedReader br = new BufferedReader(fr);
                String line;
                int compteur = 1;
                while ((line = br.readLine()) != null) {
                    if(compteur == 1) {
                        ag.setName(line);
                    }
                    if(compteur==2) {
                        ag.setSurname(line);
                    }
                    if(compteur==3) {
                        ag.setMission(line);
                    }
                    if(compteur==4) {
                        ag.setPasswd(line);
                    }
                    if(compteur>5){
                        ag.addTools(new Tools(line, ""));

                    }
                        compteur++;


                }
                fr.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return ag;


        }

    }




























