import java.util.ArrayList;

public class Agent {
    //Attributs
    private String id;
    private String name;
    private String surname;
    private String mission;
    private String passwd;
    private String idCard;
    private ArrayList<Tools> tools = new ArrayList<>();


    //Constructeur d'objets agents
    public Agent(String id, String name, String surname, String mission, String passwd, String idCard, ArrayList<Tools> tools) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.mission = mission;
        this.passwd = passwd;
        this.idCard = idCard;
        this.tools = tools;
    }





    public Agent(){

    }


    public void addTools(Tools tool){

        this.tools.add(tool); // On ajoute les tools

    }

    @Override
    public String toString() {
        return "Agent{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", mission='" + mission + '\'' +
                ", passwd='" + passwd + '\'' +
                ", idCard='" + idCard + '\'' +
                ", tools=" + tools +
                '}';
    }

    //Getters setters
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getMission() {
        return mission;
    }

    public void setMission(String mission) {
        this.mission = mission;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public ArrayList<Tools> getTools() {
        return tools;
    }

    public void setTools(ArrayList<Tools> tools) {
        this.tools = tools;
    }
}


