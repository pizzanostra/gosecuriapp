import java.io.BufferedReader;
import java.io.FileReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

public class Generateur {


    public void textToHtmlforAgents(ArrayList<Agent> ags ){

        String html="";
        for(Agent ag : ags){
           html= html + "<li> <a href=" + ag.getName() +".html>" + ag.getName() + " " + ag.getSurname() + "</a></li>"; // On récupère les informations des agents

        }

        try {
            String path = "C:\\dev\\Accueil.html";
            FileReader fr = new FileReader(path); // creation of a FileReader object fr.
            BufferedReader br = new BufferedReader(fr); // creation of a BufferedReader object br.
            String line; // declaration of an object String line.
            String accueilhtml = "";
            while ((line = br.readLine()) != null) {
                accueilhtml = accueilhtml + line;


            }
            accueilhtml = accueilhtml.replace("${agents}", html);
            System.out.println(accueilhtml);
            fr.close();
            Files.writeString(Paths.get(path), accueilhtml);
        }catch(Exception e){
            e.printStackTrace();
        }



    }


    public void generateHtml(Agent ag){



        try {
            String path = "C:\\dev\\agent_template.html";
            FileReader fr = new FileReader(path); // creation of a FileReader object fr.
            BufferedReader br = new BufferedReader(fr); // creation of a BufferedReader object br.
            String line; // declaration of an object String line.
            String accueilhtml = "";
            while ((line = br.readLine()) != null) {
                accueilhtml = accueilhtml + line;
            }
            accueilhtml = accueilhtml.replace("${name}", ag.getName());
            accueilhtml = accueilhtml.replace("${surname}", ag.getSurname());
            String htmlMaterial = "";
            for(Tools to : ag.getTools() ){ 
          if (!to.getTool().isEmpty()){
                    htmlMaterial = htmlMaterial + "" +
                            "           <div class=\"M\">\n" +
                            "    <input type=\"checkbox\" id=" + to.getTool() + " checked disabled value=" + to.getTool() + " >\n" +
                            "    <label for=" + to.getTool() + "> " + to.getTool() + "</label>\n" +
                            "    </div>\n" +
                            "\n";
            }



            }
            accueilhtml = accueilhtml.replace("${tools}", htmlMaterial);
            System.out.println(accueilhtml);
            fr.close();
            String path2 = "C:\\dev\\"+ag.getName() +".html"; // Création de la variable path
            Files.writeString(Paths.get(path2), accueilhtml); // On crée le fichier html avec répertoire de destination le path et le contenu de la variable html
        }catch(Exception e){
            e.printStackTrace();

        }



    }

}
